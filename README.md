# API LOYER VILLE

api pour chercher loyer moyen selon des critères définis 


1. [ prerequis](#install-prerequis)

    avant l'installation  verfiez les prerequis suivants
    
      * [Git](https://git-scm.com/downloads)
      * [Docker](https://docs.docker.com/engine/installation/)
      * [Docker Compose](https://docs.docker.com/compose/install/)


2. [Cloner  le project](#clone-the-project)


    cloner le projet avec la ligne de commande suivante 
    ```sh
      git clone https://gitlab.com/g.yassine/api-ville-loyer.git
    ```
3. [Demarrer l'api](#run-the-application)

    pour demarrer l'api excutez les commandes suivantes
     ```sh
      cd api-ville-loyer 
      docker-compose up -d --build
    ```

4. [utiliser un Makefile](#use-makefile) [`Optional`]
    
    pour démarrer le service
    ```sh
    make start
    ```
3. [log ](#run-the-application)

    pour voir les logs en temps réel 
    ```sh
      make log
    ```
___
___


### Images et services

* [python](https://hub.docker.com/_/python/)


les port des services :

| Server     | Port |
|------------|------|
| app        | 5050 |


###  shemas

```sh
.
├── Makefile
├── Dockerfile
├── README.md
├── requirements.txt
├── docker-compose.yml
├── app
│   ├── lib
│   │   ├── prixm2-loyer.csv
│   │   └── prixm2loyer.json
│   ├── communs.py
│   ├── config.py
│   ├── main.py

```
###  openApi
le projet est démarré,  le **port** par défaut est **5050** pour éviter tout conflit avec les ports classiques

[lien pour la doc de l'api](http://127.0.0.1:5050/docs)

###  exemple
- essonne: departement **91**
- loyer max : **1000**
- surface: **95** m2

le lien correspendant
  http://127.0.0.1:5050/ville-loyer/91/1000/95

