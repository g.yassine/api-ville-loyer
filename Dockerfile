FROM python:3.9

WORKDIR /var/projects

COPY ./requirements.txt /var/projects/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /var/projects/requirements.txt

COPY ./app /var/projects/app

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80","--reload"]
