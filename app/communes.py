from typing_extensions import Self
import urllib
from bs4 import BeautifulSoup as bs
import urllib.request
import json
import pandas as pd

class communes:
    def __init__(self, name):
        self.name="Cummuns Class"
    
    def generateResponseCommuns(url):
        responseDep = urllib.request.urlopen(url)
        data = responseDep.read()
        valuesCommuns = json.loads(data)  
        
        return valuesCommuns
    
    def getLoyerMoyen(self,code_dep,surface,loyer_max):
       prixM2souhaite=loyer_max/surface
       pathJson= './app/lib/prixm2loyer.json'
       dfs = pd.read_csv('./app/lib/prixm2-loyer.csv', sep=',')
       dfs.to_json(pathJson,indent = 1, orient = 'records' )
       loyerListFile=open(pathJson)
       dataLoyer = json.load(loyerListFile)
       data= list(filter(lambda x: x['INSEE_DEP'] == str(code_dep) and x['loyer_apparts'] <= prixM2souhaite, dataLoyer))[:30] #limit temporaire
       items = []
       for item in data:
          note = self.getNoteCommune(self,item)
          itemCommun={"desc": "-" + item["NOM_COM_M"] +" (note "+ str(note) +" et  loyer moyen de: "+ str(item["loyer_apparts"]) + ")",
                      "commune": item["NOM_COM_M"],
                      "loyer_total":item["loyer_apparts"]*surface,
                      "loyer_moyen":item["loyer_apparts"]
                      }
          items.append(itemCommun)
       items.sort(key=lambda x: x["commune"])
       return items
   
   
    def getNoteCommune(self,commune):
        urlToplateform="https://www.bien-dans-ma-ville.fr/"+commune["NOM_COM_M"].lower().replace(" ","-")+"-"+commune["INSEE_DEP"]+"-"+commune["INSEE_COM"]
        print(urlToplateform)
        try:
            response = urllib.request.urlopen(urlToplateform)
        except urllib.error.HTTPError as e:
             return "non disponible"
        except urllib.error.URLError as e:
            return "non disponible"
        else :
            result=response.read().decode("utf8")
            return self.parseNote(result)
            
    
    
    def parseNote(html):
        noteText=""
        soup = bs(html, "html.parser")
        note =soup.find("section",{"id":"avis"}).find("div",{"class":"total"})
        if note:
          noteText=note.getText()
          return noteText[0:3]
        else:
          return "non disponible"