from pydantic import BaseSettings


class Settings(BaseSettings):
    app_name: str = "Loyer  API"
    url_api_geo_base: str ="https://geo.api.gouv.fr/departements/"
    url_api_geo_attr: str ="/communes?fields=nom,code,codesPostaux,siren,codeEpci,codeDepartement,codeRegion,population&format=json&geometry=centre"
   
    items_per_request: int = 10

    class Config:
        env_file = ".env"