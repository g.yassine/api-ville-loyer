
from functools import lru_cache
from typing import Union
from fastapi import Depends, FastAPI
from fastapi import FastAPI

from app.communes import communes
from . import config

from pydantic import BaseModel

app = FastAPI()


@lru_cache()
def get_settings():
    return config.Settings()

@app.get("/")
def read_root():
    return {"app": "API LOYER"}
@app.get("/info")
async def info(settings: config.Settings = Depends(get_settings)):
    return {
        "app_name": settings.app_name,
    }

@app.get("/ville-loyer/{dep_id}/{loyer_max}/{surface}")
async def list_loyer(dep_id: int,surface: int,loyer_max: int,settings: config.Settings = Depends(get_settings)):
     urlToApi=settings.url_api_geo_base + str(dep_id) + settings.url_api_geo_attr
     valuesCommuns=communes.generateResponseCommuns(urlToApi)
     prixloyer=communes.getLoyerMoyen(communes,dep_id,surface,loyer_max)
     return prixloyer